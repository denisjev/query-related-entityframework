using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql;

namespace EFQuerying.RelatedData;

public class SplitQueriesBloggingContext : BloggingContext
{
    #region QuerySplittingBehaviorSplitQuery
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder
            .UseMySql(@"Server=localhost;Database=bdblog;Uid=root;password=root;",
                    new MySqlServerVersion(new Version(5,7,37)),
                    o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery))
            .EnableDetailedErrors();
    }
    #endregion
}